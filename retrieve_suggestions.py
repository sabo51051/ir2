
def retrieve_suggestions(keyword):
    suggestions = []
    with open('queries.txt', 'r') as file:
        lines = file.readlines()
        for line in lines:
            words = line.strip().split()
            for word in words:
                if len(word) >= 3 and word.startswith(keyword):
                    suggestions.append(word)

    return suggestions