import glob
import json
from fast_autocomplete import AutoComplete

def read_files(folder_path):
    file_paths = glob.glob(folder_path + "/*.json")
    queries = []
    for file_path in file_paths:
        with open(file_path, 'r') as file:
            for line in file:
                try:
                    json_obj = json.loads(line)
                    query_text = json_obj.get('text')
                    if query_text is not None:
                        queries.append(query_text)
                except json.JSONDecodeError:
                    pass
    return queries

def Complete(query, folder_path):
    queries = read_files(folder_path)
    words = {}
    for value in queries:
        value = value.strip()  # Remove leading/trailing whitespace
        new_key_values_dict = {value: {}}#تعيين مفتاح فارغ
        words.update(new_key_values_dict)#لحتى ضل عمحدث عالقاموس بكل مرة بلاقي فيها قيمة جديدة
    autocomplete = AutoComplete(words=words)#انشاء كائن بتمرير القاموس له
    new = autocomplete.search(query, max_cost=10, size=5)#البحث ضمن القاموس عن الكويري المدخلة
    
    w = {}#تخزين النتائج بطريقة مرتبة
    for index, value in enumerate(new):
        w[index] = value
    return w

folder_path = "C:/Users/ASUS/Desktop/New folder"
word = 'More'

results = Complete(word, folder_path)
for v in results.items():
    print(v)


import glob
import json
from fast_autocomplete import AutoComplete
from fuzzywuzzy import fuzz

def read_files(folder_path):
    file_paths = glob.glob(folder_path + "/*.json")
    queries = []
    for file_path in file_paths:
        with open(file_path, 'r') as file:
            for line in file:
                try:
                    json_obj = json.loads(line)
                    query_text = json_obj.get('text')
                    if query_text is not None:
                        queries.append(query_text)
                except json.JSONDecodeError:
                    pass
    return queries

def Complete(query, folder_path):
    queries = read_files(folder_path)
    words = {}
    for value in queries:
        value = value.strip()  # Remove leading/trailing whitespace
        new_key_values_dict = {value: {}}#تعيين مفتاح فارغ
        words.update(new_key_values_dict)#لحتى ضل عمحدث عالقاموس بكل مرة بلاقي فيها قيمة جديدة
    autocomplete = AutoComplete(words=words)#انشاء كائن بتمرير القاموس له
    new = autocomplete.search(query, max_cost=10, size=10)#البحث ضمن القاموس عن الكويري المدخلة
    
    if len(new) == 0:
        # Use fuzzy string matching to suggest corrections
        max_score = -1
        suggested_word = ''
        for word in queries:
            score = fuzz.ratio(query.lower(), word.lower())
            if score > max_score:
                max_score = score
                suggested_word = word
        if max_score > 70: # Set a threshold for the minimum score
            new = [suggested_word]
    
    w = {}#تخزين النتائج بطريقة مرتبة
    for index, value in enumerate(new):
        w[index] = value
    return w

folder_path = "C:/Users/ASUS/Desktop/New folder"
word = 'Moe'

results = Complete(word, folder_path)
if len(results) == 0:
    print('No results found.')
else:
    for v in results.items():
        print(v)



import glob
import json
from fast_autocomplete import AutoComplete
from fuzzywuzzy import fuzz

def read_files(folder_path):
    file_paths = glob.glob(folder_path + "/*.json")
    queries = []
    for file_path in file_paths:
        with open(file_path, 'r') as file:
            for line in file:
                try:
                    json_obj = json.loads(line)
                    query_text = json_obj.get('text')
                    if query_text is not None:
                        queries.append(query_text)
                except json.JSONDecodeError:
                    pass
    return queries

def suggest_alternative_queries(query, folder_path):
    # Get all the search queries from the log files
    queries = read_files(folder_path)

    # Build a dictionary of all the queries with a count of how many times they appear
    query_counts = {}
    for q in queries:
        q = q.strip().lower()
        if q in query_counts:
            query_counts[q] += 1
        else:
            query_counts[q] = 1

    # Sort the queries by their count in descending order
    sorted_queries = sorted(query_counts.items(), key=lambda x: x[1], reverse=True)

    # Use fuzzy string matching to suggest alternative queries
    suggested_queries = []
    for q, count in sorted_queries:
        ratio = fuzz.ratio(query.lower(), q.lower())
        if ratio > 70:
            suggested_queries.append(q)

    return suggested_queries

folder_path = "C:/Users/ASUS/Desktop/New folder"
query = 'Global'

suggested_queries = suggest_alternative_queries(query, folder_path)

if len(suggested_queries) == 0:
    print('No alternative queries found.')
else:
    print(f'Suggested alternative queries for "{query}":')
    for q in suggested_queries:
        print(q)



        import tkinter as tk
from fast_autocomplete import AutoComplete
from fuzzywuzzy import fuzz
import glob
import json

# Define the function to read files and create the autocomplete index
def read_files(folder_path):
    file_paths = glob.glob(folder_path + "/*.json")
    queries = []
    for file_path in file_paths:
        with open(file_path, 'r') as file:
            for line in file:
                try:
                    json_obj = json.loads(line)
                    query_text = json_obj.get('text')
                    if query_text is not None:
                        queries.append(query_text)
                except json.JSONDecodeError:
                    pass
    return queries

def suggest_alternative_queries(query, folder_path):
    # Get all the search queries from the log files
    queries = read_files(folder_path)

    # Build a dictionary of all the queries with a count of how many times they appear
    query_counts = {}
    for q in queries:
        q = q.strip().lower()
        if q in query_counts:
            query_counts[q] += 1
        else:
            query_counts[q] = 1

    # Sort the queries by their count in descending order
    sorted_queries = sorted(query_counts.items(), key=lambda x: x[1], reverse=True)

    # Use fuzzy string matching to suggest alternative queries
    suggested_queries = []
    for q, count in sorted_queries:
        ratio = fuzz.ratio(query.lower(), q.lower())
        if ratio > 70:
            suggested_queries.append(q)

    return suggested_queries

def Complete(query, folder_path):
    queries = read_files(folder_path)
    words = {}
    for value in queries:
        value = value.strip() # Remove leading/trailing whitespace
        new_key_values_dict = {value: {}}
        words.update(new_key_values_dict)
    autocomplete = AutoComplete(words=words)
    results = autocomplete.search(query, max_cost=10, size=10)
    return results

# Define the function to calculate the Levenshtein distance between two strings
def levenshtein_distance(s1, s2):
    if len(s1) < len(s2):
        return levenshtein_distance(s2, s1)
    if len(s2) == 0:
        return len(s1)
    previous_row = range(len(s2) + 1)
    for i, c1 in enumerate(s1):
        current_row = [i + 1]
        for j, c2 in enumerate(s2):
            insertions = previous_row[j + 1] + 1
            deletions = current_row[j] + 1
            substitutions = previous_row[j] + (c1 != c2)
            current_row.append(min(insertions, deletions, substitutions))
        previous_row = current_row
    return previous_row[-1]

# Define the function to suggest corrections for the user's query
def suggest_corrections(query, queries):
    suggested_words = []
    for word in queries:
        distance = levenshtein_distance(word, query)
        if distance <= 2: # Set a threshold for the maximum distance
            suggested_words.append(word)
    return suggested_words

# Define the function to handle the search
def search():
    global results # Declare "results" as a global variable so it can be accessed from other functions
    query = search_entry.get()
    previous_queries = []


   
    query = search_entry.get()
    # ... existing search code ...
      
    # Append current query to previous queries
    previous_queries.append(query)
        
    # Print previous queries under Search button    
    for q in previous_queries:
        tk.Label(window, text=q).pack()
    # Suggest corrections for the query using the Levenshtein distance
    suggested_words = suggest_corrections(query, queries)
    if len(suggested_words) > 0:
        corrected_query = suggested_words[0]
        search_entry.delete(0, tk.END)
        search_entry.insert(0, corrected_query)
        query = corrected_query
    
    # Suggest alternative queries based on the search logs
    suggested_queries = suggest_alternative_queries(query, folder_path)
    if len(suggested_queries) > 0:
        print(f"Suggested alternative queries for '{query}':")
        for q in suggested_queries:
            print(q)
    
    results = Complete(query, folder_path)
    if len(results) == 0:
        result_text.delete("1.0", "end")
        result_text.insert(tk.END, "No results found.")
    else:
        result_text.delete("1.0", "end")
        result_text.insert(tk.END, '\n'.join(str(item) for sublist in results for item in sublist))

# Create the Tkinter window
window = tk.Tk()
window.title("Search Interface")

# Create the search label and entry
search_label = tk.Label(window, text="Enter your search query:")
search_label.pack()
search_entry = tk.Entry(window)
search_entry.pack()

# Create the search buttonand the results label
search_button = tk.Button(window, text="Search", command=search)
search_button.pack()
results_label = tk.Label(window, text="Search results:")
results_label.pack()

# Create the text widget for the search results
result_text = tk.Text(window)
result_text.pack()

# Initialize the autocomplete index and the folder path
folder_path = "C:/Users/ASUS/Desktop/New folder"
queries = read_files(folder_path)
results = []

# Start the Tkinter event loop
window.mainloop()